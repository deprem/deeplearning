import tensorflow as tf
import numpy as np
from numpy import array
from keras.models import Sequential
from keras.layers import Dense

# Prepare the train data set

training_inputs = array([[1, 2],  # -> 3
                         [3, 4],  # -> 5
                         [5, 6],  # -> 7
                         [7, 8],  # -> 9
                         [9, 10],  # -> 11
                         [11, 12],  # -> 13
                         [13, 14]])  # -> 15

# Prepare the outputs
training_outputs = array([3, 5, 7, 9, 11, 13, 15])
# Outputs should be 2 dimension array format
out = training_outputs.reshape(7, 1)

# Very simple Deep Learning model creating
model = Sequential()
model.add(Dense(50, activation="relu", input_dim=2))
model.add(Dense(100, activation="relu"))
model.add(Dense(50, activation="relu"))
model.add(Dense(1))
model.compile(optimizer="adam", loss="mae")

# Fitting model
model.fit(training_inputs, out, epochs=10000, verbose=0)
# Saving model
model.save("my_model")

loaded_model = tf.keras.models.load_model('my_model')

# Giving a new input that consist of 2 numbers to predict next number in 2 dimension array format
new_input = array([21, 22])
new_input = new_input.reshape(1, 2)

print(loaded_model.predict(new_input))

converter = tf.lite.TFLiteConverter.from_keras_model(loaded_model)

tf_model = converter.convert()

open("model.tflite", "wb").write(tf_model)
